. `dirname "$(readlink -f "${BASH_SOURCE[0]}")"`/01_inPath
. `dirname "$(readlink -f "${BASH_SOURCE[0]}")"`/02_validAlNum
. `dirname "$(readlink -f "${BASH_SOURCE[0]}")"`/03_normDate
. `dirname "$(readlink -f "${BASH_SOURCE[0]}")"`/04_niceNumber
. `dirname "$(readlink -f "${BASH_SOURCE[0]}")"`/05_validInt
. `dirname "$(readlink -f "${BASH_SOURCE[0]}")"`/06_validFloat
. `dirname "$(readlink -f "${BASH_SOURCE[0]}")"`/07_validDate
. `dirname "$(readlink -f "${BASH_SOURCE[0]}")"`/08_echon
